# Folder Visualizer
1. Spin up docker compose (files in `docker` folder)
1. Need to run `script/index.js` to build a folder tree
1. Run `http-server` (must be installed globally) in root of the project
1. Open `localhost:8080` to see the visualizer


# Permissions server
1. Spin up docker compose (files in `docker` folder)
1. Need to run `script/index.js` to build a folder tree
1. Run `server/index.js` to start express
1. Make requests with a matching `companyId` & `x-allowed-resources` header to test out permissions logic

