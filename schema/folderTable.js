module.exports = {
    TableName: 'folder-table',
    KeySchema: [
        {
            AttributeName: 'folderId',
            KeyType: 'HASH',
        }
    ],
    AttributeDefinitions: [
        { AttributeName: 'companyId', AttributeType: 'S' },
        { AttributeName: 'folderId', AttributeType: 'S' },
    ],
    GlobalSecondaryIndexes: [
        {
          IndexName: 'CompanyIndex',
          KeySchema: [
            {
              AttributeName: 'companyId',
              KeyType: 'HASH',
            },
          ],
          Projection: {
            ProjectionType: 'ALL',
          },
          ProvisionedThroughput: {
            ReadCapacityUnits: 10,
            WriteCapacityUnits: 10,
          },
        },
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10,
    },
};