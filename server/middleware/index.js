const { createPermissionCheckMiddlewareById } = require('@fwi-be-common/express-middlewares');
const { SearchHelper } = require('@fwi-be-common/search-helper');
const elasticsearch = require('elasticsearch');
const { PermissionsHelper } = require('fwi-folder-permissions-helper');

const searchHelper = new SearchHelper(
    {
        host: process.env.ES_ENDPOINT,
        log: 'error',
    },
    process.env.ES_INDEX_NAME,
    process.env.ES_INDEX_TYPE
);

const esClient = new elasticsearch.Client(
    {
        host: process.env.ES_ENDPOINT,
        // awsConfig: new AWS.Config({ region: 'us-west-2' }),
        // this was required for our Lambda functions to talk to ES but does not appear to be needed here. Leaving this here for a bit until we are sure it is not necessary
        // connectionClass: require('http-aws-es') // https://www.npmjs.com/package/http-aws-es needed
        // ssl: { rejectUnauthorized: false }
    }
)



const folderPermissionCheckMiddleware = createPermissionCheckMiddlewareById({
    resourceParameterOptions: { path: 'folder_id' },
    loader: 'ES_CONTENT_LOADER',
    loaderConfig: {
        ES_ENDPOINT: process.env.ES_ENDPOINT,
        ES_INDEX_NAME: process.env.CL_INDEX_NAME,
        ES_INDEX_TYPE: process.env.CL_INDEX_TYPE
    }
});

const pathPermissionCheckMiddleware = async (req, res, next) => {
    try {
        const companyId = req.params['company_id'];
        const folderId = req.params['folder_id'];
        const resources = req.headers['x-allowed-resources'] || [];
        const allowedResources = Array.isArray(resources) ? resources : JSON.parse(resources);

        const permissionHelper = new PermissionsHelper([], allowedResources);
        if (permissionHelper.isRestrictedAccess()) {
            if (allowedResources.includes(folderId)) {
                return next();
            }

            // requested resource & allowed resources
            const startTime = new Date();
            const permissionsResources = await esClient.search({
                index: process.env.CL_INDEX_NAME,
                type: process.env.CL_INDEX_TYPE,
                body: {
                    query: {
                        bool: {
                            should: [
                                {
                                    terms: {
                                        ['id.keyword']: allowedResources
                                    }
                                },
                                {
                                    term: {
                                        ['id.keyword']: folderId
                                    }
                                }
                            ],
                            minimum_should_match: 1
                        }
                    },
                    _source: ['path', 'parent_folder_id', 'id', 'depth']
                }
            });
            const endTime = new Date();

            console.log('QUERY TOOK:')
            console.log((endTime - startTime).toString());


            const { hits: { hits, total } } = permissionsResources

            let allowedPaths = [];
            let requestedPath = {};

            for (let folder of hits) {
                const { id, path } = folder._source;
                if (id !== folderId) {
                    allowedPaths.push(path);
                } else {
                    requestedPath = path
                }
            }

            let hasAccess = false;
            for (let path of allowedPaths) {
                const pathRegex = new RegExp(path);
                if (pathRegex.test(requestedPath)) {
                    hasAccess = true;
                    break;
                }
            }
            if (!hasAccess) {
                return next(
                    new Error(
                        `access forbidden via permissionCheckMiddleware companyId: ${companyId} allowedResources: ${allowedResources}`
                    )
                );
            }
        }

        if (next) {
            return next();
        }
    } catch (error) {
        return next(error);
    }
}

const pathPermissionCheckMiddleware2 = async (req, res, next) => {
    try {
        const companyId = req.params['company_id'];
        const folderId = req.params['folder_id'];
        const resources = req.headers['x-allowed-resources'] || [];
        const allowedResources = Array.isArray(resources) ? resources : JSON.parse(resources);

        const permissionHelper = new PermissionsHelper([], allowedResources);
        if (permissionHelper.isRestrictedAccess()) {
            if (allowedResources.includes(folderId)) {
                return next();
            }

            // requested resource & allowed resources
            const startTime = new Date();
            const requestedResource = await esClient.search({
                index: process.env.CL_INDEX_NAME,
                type: process.env.CL_INDEX_TYPE,
                body: {
                    query: {
                        bool: {
                            must: [
                                {
                                    term: {
                                        ['id.keyword']: folderId
                                    }
                                },
                                {
                                    term: {
                                        ['company_id.keyword']: companyId
                                    }
                                }
                            ]
                        }
                    },
                    _source: ['path', 'parent_folder_id', 'id', 'depth']
                }
            });
            const permissionedFolders = await esClient.search({
                index: process.env.CL_INDEX_NAME,
                type: process.env.CL_INDEX_TYPE,
                body: {
                    query: {
                        bool: {
                            must: [
                                {
                                    terms: {
                                        ['id.keyword']: allowedResources
                                    }
                                }
                            ]
                        }
                    },
                    _source: ['path', 'parent_folder_id', 'id', 'depth']
                }
            });

            const endTime = new Date();

            console.log('QUERY TOOK:')
            console.log((endTime - startTime).toString());

            const { hits: { hits: requestedDocuments } } = requestedResource;
            const { hits: { hits } } = permissionedFolders;

            const allowedPaths = hits.map(folder => folder._source.path );
            const requestedPath = requestedDocuments.map(doc => doc._source.path)[0];

            let hasAccess = false;
            for (let path of allowedPaths) {
                const pathRegex = new RegExp(path);
                if (pathRegex.test(requestedPath)) {
                    hasAccess = true;
                    break;
                }
            }
            if (!hasAccess) {
                return next(
                    new Error(
                        `access forbidden via permissionCheckMiddleware companyId: ${companyId} allowedResources: ${allowedResources}`
                    )
                );
            }
        }

        if (next) {
            return next();
        }
    } catch (error) {
        return next(error);
    }
}

module.exports = {
    folderPermissionCheckMiddleware,
    pathPermissionCheckMiddleware,
    pathPermissionCheckMiddleware2
}