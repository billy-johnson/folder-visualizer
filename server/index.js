const express = require('express');
const { folderPermissionCheckMiddleware, pathPermissionCheckMiddleware, pathPermissionCheckMiddleware2 } = require('./middleware');

const bodyParser = require('body-parser'),
    compression = require('compression'),
    cors = require('cors');

require('dotenv').config({ path: __dirname + '/.env' });

const app = express();
const port = process.env.PORT || 3000;

app.use(
    cors({
        exposedHeaders: ['Location', 'Last-Modified', 'ETag', 'Content-Type']
    })
);
app.disable('x-powered-by');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(compression());
app.use(
    cors({
        exposedHeaders: ['Location', 'Last-Modified', 'ETag', 'Content-Type']
    })
);

app.get('/company/:company_id/folder/:folder_id', folderPermissionCheckMiddleware, (req, res) => {
    console.log('passed legacy permissions check');
    res.send();
});

app.get('/company/:company_id/folder/:folder_id/path', pathPermissionCheckMiddleware, (req, res) => {
    console.log('passed new permissions check');
    res.send();
});

app.get('/company/:company_id/folder/:folder_id/path2', pathPermissionCheckMiddleware2, (req, res) => {
    console.log('passed new permissions check 2');
    res.send();
});

app.get('/health-check', (req, res) => {
    res.status(200).send('Server is healthy');
});

app.use((err, req, res, next) => {
    console.error(err.stack)
    res.status(500).send('Something broke!')
})

app.listen(port);
console.log('Listening!')