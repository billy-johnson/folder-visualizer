const folderSchema = require('../schema/folderTable');
const Dynamoose = require('fwi-dynamoose').default;
const folderTableName = 'folder-table';
const dynamoose = new Dynamoose({
    endpoint: 'http://localhost:8000',
    region: 'united-states'
});
const AWS = require('aws-sdk');
const fs = require('fs');

const crypto = require("crypto");
const documentClient = dynamoose.documentClient;
const driver = dynamoose.dynamoDb();

const elasticsearch = require('elasticsearch');
const esClient = new elasticsearch.Client(
    {
        host: process.env.ES_ENDPOINT,
        // awsConfig: new AWS.Config({ region: 'us-west-2' }),
        // this was required for our Lambda functions to talk to ES but does not appear to be needed here. Leaving this here for a bit until we are sure it is not necessary
        // connectionClass: require('http-aws-es'), // https://www.npmjs.com/package/http-aws-es needed
        // ssl: { rejectUnauthorized: false }
    }
);

// const companyId = '0c4e565e-a94d-4881-8d36-1f08e1da2ce1';
const folderList = require('../folderData/devPipelineContributor.json');
const companyId = 'fe7c0680-6523-4697-bbb3-d5b3a4c5b718'; // dev-pipeline-contributor

// const folderList = require('../folderData/1000CompanyFoldersCT2.json');
// const companyId = '9f9b1118-ec15-488c-a1f5-3b7c1d0bbb32'; // 1000 company folders CT2

const buildRequestItems = (list) => {
    return list.map(item => {
        return {
            PutRequest: {
                Item: item
            }
        }
    });
};

const folderMapper = (folder) => {
    if (!folder) {
      return null;
    }
    return {
      id: folder.folderId,
      type: 'folder',
      name: folder.name || '',
      description: folder.description || '',
      company_id: folder.companyId,
      content_type: 'folder',
      sub_type: 'folder',
      parent_folder_id: folder.parentFolderId || null,
      last_modified: new Date(folder.lastModified) || '',
      created_on: new Date(folder.createdAt) || '',
      last_modified_by: folder.modifiedBy,
      created_by: folder.createdBy,
      marked_for_deletion: folder.markedForDeletion,
      folder_type: folder.folderType,
      entitlement: 1,
      content_state: 3,
      path: folder.path,
      depth: folder.depth
    };
  };

const queryParamBuilder = (companyId, parentFolderId, startKey) => {
    const params = {
        TableName: folderTableName,
        IndexName: 'CompanyIndex',
        KeyConditionExpression: `#companyId = :companyId`,
        FilterExpression: `#parentFolderId = :parentFolderId`,
        ExpressionAttributeNames: {
            '#companyId': 'companyId',
            '#parentFolderId': 'parentFolderId',
        },
        ExpressionAttributeValues: {
            ':companyId': companyId,
            ':parentFolderId': parentFolderId
        },
    };
    if (startKey) {
        params['ExclusiveStartKey'] = startKey
    }
    return params;
};

const buildBranch = async (companyId, folderMap, childFolderList, keys = [], metricsMap = {}, path = []) => {
    for (let i = 0; i < childFolderList.length; i++) {
        const folder = childFolderList[i];
        
        const { parentFolderId } = folder;
        const climbTo = keys.indexOf(parentFolderId) + 1;

        if (climbTo > -1) {
            keys = keys.slice(0, climbTo);
            path = path.length > 0 ? path.slice(0, climbTo) : [];
        } else {
            keys = [];
            path = [];
        };

        // new metadata for optimizing searches, 5 digit hex appended to "path"
        path.push(crypto.randomBytes(5).toString("hex"));
        folder['path'] = path.join(':');

        // tracking number of folders at each depth
        const depth = keys.length;
        folder['depth'] = depth;

        // done writing to folder, index to ES
        await esClient.index({
            index: 'content-library',
            type: 'cl-index',
            id: folder.folderId,
            body: folderMapper(folder)
        });

        if (metricsMap[depth]) {
            metricsMap[depth] += 1;
        } else {
            metricsMap[depth] = 1;
        };

        // maxDepth key
        const maxDepth = 'maxDepth';
        if (metricsMap[maxDepth]) {
            metricsMap[maxDepth] = metricsMap[maxDepth] < depth ? metricsMap[maxDepth] + 1 : metricsMap[maxDepth];
        } else {
            metricsMap[maxDepth] = 1;
        };

        if (keys.length === 1) {
            folderMap[folder.folderId] = folder
        } else {
            eval(`folderMap['${keys.slice(1).join("']['")}']['${folder.folderId}'] = folder`);
        };

        let childFoldersResponse = await documentClient.query(queryParamBuilder(companyId, folder.folderId)).promise();

        let nextFolders = childFoldersResponse.Items;
        let keyToStart = childFoldersResponse.LastEvaluatedKey;
        while (keyToStart) {
            childFoldersResponse = await documentClient.query(queryParamBuilder(companyId, folder.folderId, keyToStart)).promise();
            nextFolders.push(...childFoldersResponse.Items);
            if (childFoldersResponse.LastEvaluatedKey) {
                keyToStart = childFoldersResponse.LastEvaluatedKey;
            } else {
                keyToStart = undefined;
            }
        }

        if (nextFolders.length > 0) {
            keys.push(folder.folderId);
            await buildBranch(companyId, folderMap, nextFolders, keys, metricsMap, path);
        }
    }

    return folderMap;
};

const seedData = async () => {
    try {
        await driver.deleteTable({ TableName: folderTableName }).promise();
        await driver.createTable(folderSchema).promise();
    } catch (e) {
        await driver.createTable(folderSchema).promise();
    }

    const numFolders = folderList.length;
    for (let chunk = 0; chunk < numFolders; chunk += 25) {
        const chunkedList = folderList.slice(chunk, numFolders - chunk < 25 ? numFolders : chunk + 25);
        await documentClient.batchWrite({
            RequestItems: {
                [folderTableName]: buildRequestItems(chunkedList)
            }
        }).promise();
    }
}

const buildTree = async (companyId) => {
    const rootFolderResult = await documentClient.query({
        TableName: folderTableName,
        IndexName: 'CompanyIndex',
        KeyConditionExpression: `#companyId = :companyId`,
        FilterExpression: `#root = :root`,
        ExpressionAttributeNames: {
            '#companyId': 'companyId',
            '#root': 'root',
        },
        ExpressionAttributeValues: {
            ':companyId': companyId,
            ':root': true
        },
    }).promise();

    if (rootFolderResult.Count < 1) {
        throw Error('no root folder')
    }

    const rootFolder = rootFolderResult.Items[0];

    let path = [];
    let keys = [rootFolder.folderId];
    // new metadata for optimizing searches, 5 digit hex appended to "path"
    path.push(crypto.randomBytes(5).toString("hex"));
    rootFolder['path'] = path.join(':');

    // tracking number of folders at each depth
    rootFolder['depth'] = 0;

    // done writing to folder, index to ES
    await esClient.index({
        index: 'content-library',
        type: 'cl-index',
        id: rootFolder.folderId,
        body: folderMapper(rootFolder)
    });

    let childrenOfRoot = await documentClient.query(queryParamBuilder(companyId, rootFolder.folderId)).promise();

    let nextFolders = childrenOfRoot.Items;
    let keyToStart = childrenOfRoot.LastEvaluatedKey;
    while (keyToStart) {
        childrenOfRoot = await documentClient.query(queryParamBuilder(companyId, rootFolder.folderId, keyToStart)).promise();
        nextFolders.push(...childrenOfRoot.Items);
        if (childrenOfRoot.LastEvaluatedKey) {
            keyToStart = childrenOfRoot.LastEvaluatedKey;
        } else {
            keyToStart = undefined;
        }
    };

    if (nextFolders.length > 0) {
        const metricsMap = {};
        const tree = await buildBranch(companyId, rootFolder, nextFolders, keys, metricsMap, path);

        return {
            tree,
            metrics: metricsMap
        }
    } else {
        return rootFolder;
    }
};

const mapToD3 = folderTree => {
    const neededFields = ['folderId', 'parentFolderId', 'name', 'parent', 'children', 'depth', 'path'];
    const uuidRegex = /[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/;
    folderTree['children'] = [];
    for (key in folderTree) {
        if (uuidRegex.test(key)) {
            folderTree.children.push(folderTree[key]);
            delete folderTree[key];
        } else {
            if (key === 'parentFolderId') {
                folderTree['parent'] = folderTree[key];
            } else if (!neededFields.includes(key)) {
                delete folderTree[key];
            }
        }
    }
    if (folderTree.children.length > 0) {
        folderTree.children.forEach(childFolder => {
            mapToD3(childFolder);
        });
    } else {
        delete folderTree.children;
    }
    return folderTree;
}

(async () => {
    try {
        await seedData();
        const folderTree = await buildTree(companyId);
        const D3TreeData = mapToD3(folderTree.tree);
        fs.writeFile('./folderData/treeData.json', JSON.stringify(D3TreeData), err => {
            if (err) {
                console.error(err);
                return;
            }
        });

        fs.writeFile('./folderData/treeMetrics.json', JSON.stringify(folderTree.metrics), err => {
            if (err) {
                console.error(err);
                return;
            }
        });
    } catch (e) {
        console.log(e)
    }
})();
